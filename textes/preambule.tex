\ClearForPaper
\chapter*{Préambule}
\addcontentsline{toc}{chapter}{Préambule}
\markboth{Préambule}{Préambule}

	\epigraph{(l'indicible n'est pas tapi dans l'\ecriture{}, il
		est ce qui l'a bien avant déclenchée)}
		{\cite[Georges][63]{perec_1975}}


\textit{Comment} peut-on commencer à écrire~? L'acte
inaugural de la rédaction d'un manuscrit est énigmatique. La force qui vectorise l'\ecriture{} l'est tout autant~:
pourquoi, mais surtout \textit{pour qui} écrit-on~?

Avant de démarrer ce long voyage jusqu'à la dernière ligne droite, dans
cet espace liminaire, je propose au lecteur de penser ensemble ces deux
questions.

Tout texte doit bien commencer par un premier mot, par une première
lettre, gravée sur le feuillet -- en papier ou en pixels, peu importe.
Tout manuscrit est adressé -- à quelqu'un, à plusieurs.

Certes, la vie d'une \ecriture{} commence bien avant l'inscription du
premier signe qui l'actualise. Dans un certain sens, elle est toujours
déjà commencée, dans d'autres \ecriture{}s, dans d'autres vies, dans
d'autres temporalités que celles de l'auteur. Un processus d'\ecriture{}
est inévitablement pris dans une trame d'autres textes, qui le tissent
plus ou moins secrètement, et qui le rendent lisible, comme une étoile
dans sa constellation. Les philosophes médiévaux l'avaient bien compris et ils reconnaissaient volontiers leur dette vis-à-vis des penseurs
anciens, comme l'atteste ce petit aphorisme, attribué à Bernard De
Chartres \parencite*{De_Chartres_1130}~: «~Nous sommes
comme des nains sur les épaules des géants~». Et, si l'idée d'avoir des
colosses pour prédécesseurs peut rencontrer notre scepticisme,
n'oublions pas que plusieurs générations de nains qui se tiendraient
fermement les uns sur les épaules des autres, atteindraient
théoriquement la taille d'un géant. Cette image, à mi chemin entre comique
et héroïque, pourrait représenter la structure d'une puissance très
humaine~: la \transmission{} du savoir, toujours précaire, par la culture.

Quant à la deuxième question, celle de l'adresse de l'\ecriture{}, elle
n'est guère plus simple. Quels lecteurs sont conviés à participer des
inventions, des doutes, des devenirs qui traversent et rendent vivant un
manuscrit~? Là encore, les appartenances et
les coordonnées de l'auteur se mélangent à celles de tant d'autres
sujets, groupes, institutions\ldots{} Et, tout comme il serait
invraisemblable de déterminer de façon exhaustive les sources d'un
manuscrit, en repérer tous les destinataires ne serait pas plus aisé.

Ce n'est pas le propos de ce préambule, je n'ai pas la capacité ni
l'intention de décortiquer ces deux énigmes, le \emph{d'où} et le
\emph{vers où} de cette thèse. Je voudrais seulement les pointer, comme
on pointe le nord et le sud sur une carte, car je m'aperçois que ces
coordonnées sont agissantes dans ce processus d'\ecriture{}~: elles
exposent ce travail à l'épreuve de ses propres lacunes -- et de celles
de son auteure.

Malgré la non-ponctualité de l'origine de la vie d'un texte, malgré
l'ouverture de ce dernier à plusieurs destinataires et à de différentes
lectures, le geste qui actualise l'\ecriture{}, lui donnant une forme
précise, n'est tout de même pas anodin. Au contraire, lorsque l'auteur
s'y prête, cet exercice peut lui faire éprouver le vertige de sa propre
insuffisance.

Commencer à écrire, id est, s'attaquer à composer des phrases, à partir
d'une collection bien définie de signes, imbibé d'un ensemble beaucoup
moins défini d'expériences, mû par un enchevêtrement d'interrogations
irrésolues, conscientes et inconscientes, et en direction d'une
multiplicité de destinataires. Commencer à écrire est un geste hésitant.
Singulier et pluriel à la fois, cet acte est chargé d'affects, tristes
et joyeux\footnote{Je fais référence, ici, à la théorie des
passions élaborée par Baruch Spinoza \parencite*{Spinoza_1677}.}.

Affects tristes car, par ce commencement, l'écrivain établit son
renoncement à l'autre texte, à l'autre possibilité de dire et d'être
entendu, à toute \ecriture{} différente qu'il aurait pu actualiser, mais
qu'il accepte de perdre, la laissant parmi les utopies et les uchronies
qui parsèment la vie de chacun.

Ce renoncement, qui n'est pas sans lien avec une forme de deuil, libère
chez l'auteur des affects joyeux~: le premier mot, la première lettre,
délivre l'écrivain de l'emprise paralysante de la page blanche.
L'environnement de son texte peut ainsi entrer en mutation, d'entité
muette et angoissante, il se transforme en espace-temps potentiel.
L'inscription de la première lettre est à la fois une preuve et une
promesse d'une puissance~subjective fondamentale~:
\textit{faire trace}.

Reprenant la réflexion de Jean-François Chiantaretto sur l'\ecriture{}, et
en particulier sur l'\ecriture{} autobiographique -- et je considère
qu'une thèse a d'importants aspects en commun avec un texte
autobiographique -- nous pouvons penser cette dernière comme un «~lieu
psychique intermédiaire~»
\parencite[45]{Chiantaretto_2015}. Cela signifie que,
d'une part, le «~lieu~» de l'\ecriture{} se situe à l'articulation entre
l'intra-psychique, l'\intersubjectif{} et le social, et que, d'autre part,
cet espace préfigure la possibilité d'héberger l'expérience subjective
en tant qu'expérience créative. Le geste inaugural de l'\ecriture{}
me paraît donc se donner comme un acte d'accueil. Pas question de
commencer à écrire, avant d'avoir fait une place, en soi, à la venue de
l'inconnu, de l'inattendu d'une pensée en devenir, dont on ne connaît
pas l'essor. L'\ecriture{} est intimement nouée à l'écoute -- tout d'abord
en tant qu'espoir de celui qui écrit d'être écouté, ou, comme le dit
Chiantaretto, en tant que «~croyance nécessaire pour chaque sujet, dans
sa difficile et solitaire singularité, qu'il y a bien quelqu'un pour
l'écouter~» \parencite{Chiantaretto_2015}.
À partir de cette croyance, l'\ecriture{} ouvre une puissance d'écoute,
dont l'écrivain est à la fois le \emph{sujet}, le \emph{lieu}, et
l'\emph{objet}.

L'écrivain est un \emph{sujet} d'écoute, car l'activité d'écrire est
faite de longs moments d'attente, où il s'assoit, pour ainsi dire, sur
le seuil d'une porte entrouverte~: si, pris d'impatience, il l'ouvrait
brusquement pour faire irruption, il ne trouverait qu'un espace vide,
terne, fui par le moindre signe, par le moindre indice d'un sens. Alors
il est obligé d'attendre, de se faire silencieux et écouter avec tous
ses nerfs les mouvements discrets derrière la porte. Ces derniers sont
d'abord à peine perceptibles, mais, si la disponibilité de l'écrivain à
écouter ne fléchit pas, des mots commencent à résonner. Et pas seulement
des mots~: suspensions, changements de rythme, sursauts, pirouettes,
ouvertures, interruptions, variations de ton\ldots{} L'\ecriture{} naît
dans une sonorité, elle se met en route lorsque le silence cesse d'être
muet.

L'écrivain est un \emph{lieu} d'écoute, car, au-delà du seuil où il
reste à l'affût, se cache un lieu interne, un espace de recueillement et
de contenance. Réceptacle, caisse de résonance de l'intime et de
l'immanent, cet espace interne fonctionne comme une membrane où
l'universel et le transcendant laissent parfois une empreinte. Dans les
mots (mono-vocaliques) de Georges Perec, «~je cherche en même temps
l'éternel et l'éphémère~» \parencite[657]{perec_1978}.

L'écrivain, est, enfin, l'\emph{objet} de l'écoute qui sous-tend son
\ecriture{}, ce qui est tautologique lorsqu'on parle d'un texte animé par
une quelque tendance autobiographique. Mais il serait plus précis de
dire que l'auteur n'est que l'un des objets de son \ecriture{}, ou encore,
que, pour écrire, il est amené à se mettre à l'écoute d'une multiplicité
d'objets qui le concernent, ou qui touchent à ce qui pour lui fait
énigme.

Parmi les différents objets que je tenterai d'écouter dans les pages
qui suivent, j'ai gardé une place particulière pour celui qui est le
moteur de tous les autres~: l'\textit{accueil de l'étranger}. De
l'étranger en soi, hors de soi, dans son corps, dans le corps social,
dans un groupe, dans la possibilité d'un \temoignage{}. Que fait-on de
l'étranger~? «~On~», c'est à dire nous, les sujets humains, chacun face
à ses étrangers intérieurs, ou à ses semblables dans l'étranger.
L'«~étranger~», c'est à dire, tout ce qui vient d'ailleurs, loin dans
le temps et dans l'espace, cette entité inquiétante et familière,
et d'autant plus étrange lorsqu'elle s'enchevêtre avec une identité
qu'on croit posséder. L'étranger~: l'irréductible à l'uniformité, si
présent en chaque groupe et en chacun, si dérangeant, que nous n'avons
cesse de le projeter dans le royaume du dehors et de l'autre.

La possibilité d'une forme d'accueil (psychique, social et politique) de
l'étranger, est, je crois, profondément nouée au
\textit{travail du \feminin{}}, et plus particulièrement,
au \feminindeliaison{}\label{feminin_liaison}. Nous allons rencontrer à
plusieurs reprises ces deux notions, qui jouent un rôle central dans ce
travail. Essayons donc d'en donner un premier aperçu.

Par \textit{travail du \feminin{}}, Gérard Bazalgette
entend «~ce qui permet une première élaboration de l'activité-passivité
originaire, là où elle devient mortelle pour le sujet~»
\parencite[78]{Bazalgette_2003}. Pour cet auteur, grâce
au \textit{travail du \feminin{}}, le sujet peut instituer
(et réinstituer en permanence) un espace potentiel de
\textit{séparation--réunion}, en dehors duquel il serait
sous l'emprise des pulsions phalliques, dans leurs tenants actifs et
passifs -- celui de l'expansion illimitée et celui de l'autosuffisance du
Moi. Avec Georges Gaillard et Jean-Pierre Pinel
\parencite*{Gaillard_Pinel_2012}, nous pourrions comprendre
ces deux poussées phalliques comme des expressions du \emph{désir d'être
tout}, selon la belle formule de Georges Bataille
\parencite*{Bataille_1943}, désir qui n'en finit pas de
chercher des limites, et qui donc «~requiert d'être en permanence subverti par le \feminin{}~»
\parencite[126]{Gaillard_Pinel_2012}. Similairement,
Jacqueline Schaeffer \parencite*{schaeffer_instable_2002}
décrit cette activité comme une force psychique d'articulation entre les
deux sexes, préservant leur relation et leur différence -- en un seul
mot, leur conflictualité. Une autre façon de saisir ce travail psychique
fondamental est proposée par Dominique Cupa
\parencite*{Cupa_2007}, qui le rapproche de la tendresse, en tant que pulsion d'autoconservation qui concerne la peau, la contenance et le tissage. Selon cette auteure, l'attente maternelle, avec sa puissance d'acceptation et de contenance, est à même de transformer la cruauté du bébé, ouvrant la vie psychique de
l'\emph{infans} à la possibilité d'une rencontre avec l'autre. Je
voudrais souligner que, lorsqu'on parle du
\textit{travail du \feminin{}}, et, plus largement, du
\emph{\feminin{}}, toute définition positive est à traiter avec
circonspection. Il est en effet quelque peu paradoxal d'affirmer, d'ériger
une force psychique qui, par définition, est opérante en
creux, dans une forme d'abstinence qui laisse la place à l'autre. En
effet, le \emph{\feminin{}} n'est pas vraiment pensable en soi, mais
seulement à partir de son intrication avec le phallique, qu'il contient
et qu'il met en transformation. Remarquablement, le
\textit{travail du \feminin{}} est toujours aux prises avec
un point de butée, celui que Sigmund Freud appelle «~refus du \feminin{}~»
\parencite{freud_AnalyseAvecSansFin_1937}, et qu'il se
représente tel un «~roc~», qui est à la fois une instance originaire et
ultime, définissant tout ce qui s'oppose au pulsionnel et à l'étranger
\parencite{schaeffer_refus_2013}. Ce «~roc~» est l'écueil
contre lequel les processus de \subjectivation{} et les efforts du
«~travail de l'humain~»\footnote{Je revoie le lecteur vers la réflexion de Nathalie Zaltzman \parencite*{zaltzman_guerison_1999} sur la \Kulturarbeit{}, processus d'humanisation du monde, qui se déroule dans la cure analytique et dans l'aventure de tout sujet. La pensée de la \textit{Kulturarbeit} aura une place centrale dans ce travail.} sont destinés à
s'enliser, encore et encore, et à devoir toujours recommencer. Il s'agit
d'une forme de résistance à l'analyse que Freud expérimente dans la
rencontre avec ses patients, et qu'il pense irréductible, car ancrée au
niveau biologique. Le \textit{refus du \feminin{}} est opérant tant dans
les hommes que dans les femmes~: il s'apparente du refus de castration,
de la négation de la différence des sexes et du fantasme de bisexualité.
Selon Schaeffer, ce roc indépassable s'érige sur la peur ancestrale
d'«~un sexe \feminin{} invisible, secret, étranger et porteur de tous les
fantasmes dangereux~»
\parencite[3]{schaeffer_instable_2002}. Quels dangers
évoquent de tels fantasmes~? Effraction, perte de contrôle, perte de
pouvoir, humiliation des illusions de maîtriser le monde\ldots{} Le
signifiant de \emph{\feminin{}} condense en soi toutes ces menaces, et, en
le refusant, le sujet récuse la possibilité d'«~être défait~»
\parencite{schaeffer_refus_2013}.

Cette remarque nous amène à considérer la deuxième notion que l'accueil
de l'étranger nous évoque, celle du \feminindeliaison{}. Par
cette expression, Georges Gaillard
\parencite*{gaillard_2008} désigne une potentialité
psychique qui permet à un sujet ou à un groupe d'accueillir et
transformer sa propre destructivité et celle de l'autre. Une telle
potentialité peut émerger à deux conditions~: la tolérance au
\emph{\feminin{}} et aux inquiétudes dont il est porteur, et l'adossement à
une instance incarnant quelque chose du \emph{père --} une
référence partagée, une verticalité. Le \emph{\feminin{} de liaison
}est la disponibilité subjective ou groupale à faire une place en
soi pour l'autre, à accueillir en soi les poussées phalliques
narcissiques et leurs excès mortifères (excès de \deliaison{}, meurtriers,
ou excès de liaison, incestueux), à lier la force libidinale et à opérer
une différenciation symbolisante, qui ouvre à la transformation des
contenus archaïques -- ceux du sujet ou du groupe, et ceux de l'autre.
Il est alors question de «~se prêter à la \deliaison{}~»
\parencite[119]{gaillard_2008}, acceptant d'être
utilisé par l'autre, et que celui-ci soit témoin de sa propre
vulnérabilité. Cette potentialité psychique me paraît alors exprimer
quelque chose d'une \emph{puissance de l'impuissance}, une force de
\emph{patience}, dans toute la polysémie de ce mot -- ajournement de la
satisfaction du besoin, attente, passivation, passion. Comme Gaillard le
souligne, il est possible de faire une analogie entre le travail du
\emph{\feminin{} de liaison} et le masochisme érogène psychique, qui
permet au sujet de supporter la grande quantité d'énergie libidinale
libérée par la jouissance~: le \emph{\feminin{} de liaison}, comme le
masochisme, fonctionne comme une puissance de cohésion souple, du Moi ou
du groupe, qui prépare ces derniers à une effraction qui, sans
l'activation de cette qualité malléable, pourrait les détruire. Une
telle puissance de l'impuissance, force furtive qui opère en secret,
n'est pas sans évoquer l'activité du \emph{travail du négatif\label{travail_neg}.
}\footnote{Nous aurons le temps d'investiguer cette
notion dans la suite de ce manuscrit, mais soulignons d'ores et déjà que
le \emph{négatif} de la psychanalyse n'est pas le \emph{négatif} de la philosophie, et notamment celui qui est au cœur de la pensée de
Georg W. F. Hegel. Si ce dernier établit
une négativité dialectique, destinée à l'\emph{Aufhebung} (négation de
la négation, dépassement du négatif), en psychanalyse le négatif ne peut
pas fonctionner de cette manière, car l'inconscient ignore les lois de
la logique, et laisse coexister les opposés~: le conflit de deux
instances ou de deux contenus psychiques ne donne jamais lieu à une
synthèse pacifiante, mais plutôt à une formation de compromis, instable
et souvent douloureuse. Le rapport entre Freud et Hegel mérite donc
d'être problématisé, comme le montre un bel article de Claire Pagès
\parencite*{Pages_2015}}. Le\emph{négatif} psychanalytique, introduit par Freud dès le début de ses recherches, concerne l'ensemble des défenses primaires (refoulement, désaveu, forclusion, négation). André Green, qui reprend la pensée freudienne du
négatif et qui propose d'en explorer le \emph{travail}, souligne que cet
ensemble de défenses permet à l'appareil psychique d'émettre un
jugement, de faire un choix, entre l'existence et l'inexistence de
l'objet d'une pulsion, d'une perception, ou d'une représentation.
\parencite{green_idees_2002, green_negatif_1993, green_negatif_1995}. Le \emph{travail du négatif} s'accomplit ainsi à travers un sacrifice, inévitable pour le
sujet, qui ne pourrait rien penser, rien éprouver, et qui ne pourrait
pas vivre aux prises avec une réalité totalement affirmée. Dans les mots
d'André Green, «~l'idée d'un \guill{en moins} renvoie en fait à celle d'un
\guill{en trop}. Il y a trop de tout: trop de besoin, trop de désir, trop de
manque~». \parencite[26]{green_negatif_1995}. Le
\emph{négatif} exerce alors une importante fonction de pare-excitations,
en dehors de laquelle aucun processus de métabolisation et de
symbolisation ne serait concevable, à tel point que, comme Freud le
suggère à propos de l'hallucination négative
\parencite{freud_Complement_Metapsy_1917d}, on pourrait
penser que la compréhension de cette activité silencieuse est
fondamentale pour étudier tout travail psychique «~positif~».

Si, dans cet espace-temps suspendu sur le seuil du manuscrit, j'ai
décidé d'introduire ces différentes nuances du \emph{\feminin{}} (travail,
refus, capacité de liaison, tolérance de la \deliaison{}) et du
\emph{négatif}, c'est que, tout au long de ma pratique de terrain, elles
ont été des nécessaires compagnes de chemin. Nécessaires mais
silencieuses, pendant un certain temps. Jusqu'à ce que le \emph{lieu
intermédiaire} qui s'ouvre dans ces lignes parvienne à leur restituer
une voix, des mots. Comme si la problématique de l'accueil de l'étranger
-- que j'avais élaborée, au premier abord, d'un point de vue
sociopolitique, et qui était devenue pour moi, au moins partiellement,
un objet militant -- m'avait amenée à tourner en rond. Surgie d'abord au
plus vif de ma praxis clinique, elle s'est éloignée de moi, devenant
plus abstraite, puis elle s'est absolutisée en une «~\tacheprimaire{}~» au
nom de laquelle j'ai travaillé et j'ai lutté, mais dont j'ai
paradoxalement commencé à perdre de vue tout ce qu'elle portait de non
absolu -- de fragile, de vivant, de processuel, et justement, de
problématique. Enfin, elle s'est laissée retrouver, ou elle m'a
retrouvée, elle s'est montrée à moi à nouveau, en tant qu'objet de
recherche émergeant des aspérités de certaines rencontres cliniques qui
ont précédé et déclenché cette thèse, et ensuite transformé par
l'expérience de presque trois années d'enquête. Je commence donc ce
travail de rédaction avec le sentiment d'avoir tourné autour de mon
objet, mais pas vraiment en rond~: je dirais, plutôt, en spirale.

En somme, peut-être bien qu'afin de pouvoir compléter ce mouvement en
spirale, j'ai dû écrire ce préambule. Comme une sorte de recensement, ou
de recueillement, comme un moment ou un lieu où l'on s'attarde, où l'on
hésite, procrastinant le début de la ballade. Je me rends bien compte
que cette \ecriture{} marginale est probablement plus importante pour
l'auteure que pour les lecteurs, mais je préviens ceux qui vont se
promener avec moi dans les pages qui suivent~: il y aura d'autres
divagations, d'autres parenthèses pour protéger les phénomènes
minuscules, les rencontres, les espoirs, la vie \infraordinaire{}
\emph{d'où} cette thèse puise son sens, \emph{vers où} cette \ecriture{}
tend à faire retour.

\ClearForPaper
\bookmarksetup{startatroot}% exit last part (toc parent is root)
\addtocontents{toc}{\bigskip}% some space to show exit
\chapter*{Conclusion et perspectives}
\addcontentsline{toc}{chapter}{Conclusion et perspectives}
\markboth{Conclusion et perspectives}{} % headers
\stepcounter{chapter} % increment chapter counter as conclusion is a hidden chapter

\epigraph{Ainsi chercheurs et migrants auraient en commun le sens de la quête, la quête de sens,
et le besoin de trouver une place au sein d'une fratrie sans cesse renouvelée.}
    {\cite[228]{Bruyere_2014}}

Sur le seuil de cette partie conclusive, l'élan de l'\ecriture{} devient timide. Une certaine gêne infiltre la démarche de la rédaction~: au bout d'un travail de trois ans, un travail qui a demandé une implication si dense, il faudrait être en mesure de dire des choses importantes, il faudrait avoir, enfin, du savoir à transmettre. Bien au contraire, je me sens, aujourd'hui, encore moins \guil{savante} qu'au début de cette enquête. Par contre, le processus d'\ecriture{} a laissé des traces~: le lecteur a parcouru jusqu'ici celles qui ont bien voulu s'inscrire sous forme de graphèmes, mais d'autres traces, je les porte en moi -- ou, plus précisément, elles se sont imprimées dans l'espace des liens entre moi et mes objets de recherche. Dans cet \textit{espace entre}, dans cet \textit{interstice}, ont pu venir s'abriter les signes d'une expérience, qui m'a changée, en tant que clinicienne, chercheuse et militante, qui a transformé mon être-au-monde. Une expérience, plutôt qu'une connaissance~: voici l'objet que je souhaite laisser au lecteur, dans ces dernières pages.

\section{Le préconscient et l'intersubjectivité à l'épreuve des frontières}

Même si, jusqu'ici, je ne l'ai jamais nommé, le \textit{\preconscient{}} est le plus important parmi les \guil{espaces interstitiels} dont cette thèse rend compte~: hormis le travail du \textit{\preconscient{}}, aucun \guil{interstice} ne saurait s'offrir au sujet. La notion de \guil{\preconscient{}} (Pcs) a été élaborée par Freud, qui l'a longuement reprise et remaniée. L'œuvre de Freud nous lègue, en effet, deux différentes façons d'entendre le \textit{\preconscient{}}~:

\begin{enumerate}
    \item Le \preconscient{} de la première topique \parencite{Freud_1900}. Il s'agit d'un lieu psychique intermédiaire entre l'inconscient et le conscient. On peut se le figurer comme un pont, par lequel les représentations de choses doivent passer, afin de devenir des représentations de mots.
    \item Le \preconscient{} de la seconde topique \parencite{freud_moi_ca_1923}. Ici, Freud incorpore le \preconscient{} à l'instance du Moi~: le \preconscient{} est une \guil{qualité} du Moi, qui consiste à lier l'énergie pulsionnelle, permettant ainsi au Moi de connecter entre elles représentations de choses et représentations de mots.
\end{enumerate}

Même si elles ne concernent pas explicitement le \guil{\preconscient{}}, les propositions théoriques de Wilfred R. Bion \parencite*{Bion_Thinking} autour de la \guil{\fonctionalpha{}} de la mère, disent quelque chose de fondamental d'une telle \guil{qualité} du Moi~: il est question de \guil{détoxifier} les \guil{proto-pensées du bébé}, de psychiser ces contenus encore très somatiques, et de les rendre \guil{pensables}, par le biais d'une disponibilité à les contenir, à les accueillir dans une \guil{capacité de rêverie}. Une telle capacité n'émerge pas \guil{spontanément}, mais découle de la relation avec un autre, qui l'exerce avec et pour le petit humain. Il s'agit, précisément, de la \guil{capacité} du \textit{\preconscient{}}.

Dans ce manuscrit, il a été question, à plusieurs reprises, de l'élaboration de Piera Aulagnier \parencite*{Aulagnier_1975} autour de l'\textit{originaire} et du \textit{\pictogramme{}} -- ce \guil{fond représentatif}, \guil{source somatique de la représentation psychique du monde}, qu'il est question de \guil{métaboliser}, pour que les éprouvés corporels puissent trouver une figuration. Une telle élaboration permet à Aulagnier de poursuivre la pensée freudienne et bionienne du \textit{\preconscient{}}, mettant en exergue l'importance fondamentale de l'\textit{\intersubjectivite{}}. Pour qu'un éprouvé corporel puisse se métaboliser, le sujet ne peut pas rester seul~: c'est justement sur la scène de cette première forme de liaison, entre Soma et Psyché, que l'Autre fait son apparition, en tant que \guil{porte-parole} du sujet.

René Kaës a étudié le travail du \preconscient{} dans les groupes, nous donnant ainsi des importants indices sur la relation entre \textit{\preconscient{}}, \textit{\intersubjectivite{}} et \textit{langage}. Pour cet auteur, le \preconscient{} est noué à l'\guil{entre-dire}, le lieu relationnel des fonctions interprétatives et contenantes~:

\begin{quote}
\guil{L'entre-dire \intersubjectif{} est une des conditions de l'avènement de la parole, du dire avec la parole ; il est aussi l'effet des entre-dits intimes capables de se nouer avec d'autres dire, ce qui suppose quelques zones de passage entre les lieux psychiques du dedans et du dehors, et d'abord quelques formations identiques entre les sujets entre-disants.} \parencite[18]{Kaes_2010}
\end{quote}

Nous pouvons donc entendre le \preconscient{} comme un \guil{espace \interstitiel{}} entre le corporel et le psychique, et entre le sujet et l'Autre~: sur la \guil{scène} du \preconscient{}, joue l'\intersubjectivite{}. Seulement grâce à l'ouverture d'une telle \guil{scène}, le sujet peut arriver à lier les contenus bruts qui le traversent, à les mettre en sens, et à tisser les fils qui intriquent (synchroniquement et diachroniquement) sa vie à celles des autres. Voici donc pourquoi je tiens à parler de \textit{\preconscient{}} dans ces pages conclusives~: pour les sujets issus de l'\ordaliemigratoire{}, cet espace interstitiel est le lieu d'une douleur, d'une déchirure psychique qui remonte à leur préhistoire, et qui se réactualise, aux prises avec le \textit{\deshumain{}} qui informe les dispositifs \necropolitique{}s jalonnant les frontières. J'invite le lecteur à relire les hypothèses qui structurent ce travail, et notamment la sous-hypothèse 1.1~:

\paragraph{H1}

 \textbf{Sous-hypothèse 1.1}~: La \textit{\geographiepsychique{}} ouverte par l'élan d'\guil{\alloengendrement{}} est travaillée par l'Histoire, singulière et collective. Le sujet part se remettre au monde \textit{ailleurs}, suivant une cartographie intérieure, imprimée sur une surface psychique sensible, qui a été précocement \guil{impressionnée} par une succession complexe de \textit{catastrophes} dans l'histoire -- individuelle, familiale et sociale.

La \guil{cartographie intérieure, imprimée sur une surface psychique sensible} (que je mets en relation avec le \registreoriginaire{} de la vie subjective) relève de la \guil{scène} \preconscient{}e. La \guil{\geographiepsychique{}} est une façon d'entendre la travail du \preconscient{}, en ce qu'il cherche à établir des coordonnées pour que le Je puisse s'orienter dans le monde (à l'articulation entre sa réalité psychique et la réalité extérieure). Lorsque les premiers objets du petit d'homme négligent leur rôle de \guil{cartographes}, au service du jeune explorateur, la trajectoire existentielle du Je sera probablement celle d'une \textit{errance} qui, dans certains cas -- à condition que le sujet ne s'enferme pas dans un mythe d'\textit{\autoengendrementtraumatique{}} trop \guil{satisfaisant}, et à condition qu'il puisse dialectiser ce fantasme clôturant, le mettant en tension avec un élan d'\textit{\alloengendrement{}} -- pourra donner lieu à un \textit{\travaildexil{} psychique}.

Comme nous l'avons vu, entre l'\textit{errance} et l'\textit{exil psychique} des sujets que j'ai pu accompagner, se déploie le/la geste de l'\textit{\ordaliemigratoire{}}. Or, par rapport aux hypothèses que j'ai formulées, il me semble que la prise en compte de la clinique rend nécessaire un temps de réflexion supplémentaire concernant le rapport entre l'\ordaliemigratoire{} et le \preconscient{}. Dans ma sous-hypothèse 1.3, j'énonce que~:

\paragraph{H1}

 \textbf{Sous-hypothèse 1.3}~: L'\guil{\ordaliemigratoire{}} est, pour le sujet, une manière d'accomplir deux gestes dont dépend sa survie psychique~:
\begin{enumerate}
    \item se déprendre d'un \guil{\espaceparlant{}} qui n'a pu offrir au Je aucun habitat viable, un espace incapable de l'accueillir, de l'identifier, d'ouvrir sa temporalité subjective au passé et à l'avenir, et de le tenir au monde, l'emprisonnant dans un présent sans issue -- aplati par un déterminisme et par un fonctionnement opératoire, qui taisent ou banalisent les catastrophes historiques.

    \item ébaucher une forme de représentation de ces catastrophes -- les inscrire, tout d'abord, dans une sensori-motricité, dans une trajectoire qui investit l'espace externe -- sous forme de voyage sans retour et potentiellement fatal (ordalie), permettant au sujet de mettre en scène son exclusion radicale. Celle-ci devient, dans ce cadre, une expérience héroïque d'exclusion de la commune humanité, par laquelle le sujet revendique un statut d'\guil{\exception{}}, dans le cadre d'un \guil{\autoengendrementtraumatique{}}.
\end{enumerate}

\textit{Se déprendre d'un espace incapable d'accueillir, ébaucher une représentation des catastrophes}~: ces deux \guil{gestes}, centraux dans l'\ordaliemigratoire{}, concernent, évidemment, le \preconscient{} et l'\intersubjectivite{} en ce qu'ils ont de \textit{vital}, c'est à dire, les capacités de liaison du sujet. Mais \textit{quid} d'un \preconscient{} et d'une \intersubjectivite{} qui ne fonctionnent plus que sous le sceau de la \necropolitique{}~? \textit{Quid} d'une \guil{capacité de rêverie} à l'envers, une \textit{incapacité} hostile, qui abandonne le sujet à un glissement indéfini vers des zones de mise à mort~? Il est fondamental de se poser ces questions, car l'\ordaliemigratoire{} est attenante à ce \preconscient{}, à cette \intersubjectivite{} \necropolitique{}. Il n'est pas seulement question d'une tentative, de la part du sujet, de \guil{mettre en scène} (c'est à dire, \guil{\preconscient{}iser}), par le/la \gesteordalique{}, son \guil{expérience héroïque d'exclusion de la commune humanité}. Comme l'accompagnement clinique d'Ismaël le montre bien, l'\ordaliemigratoire{} est aussi un trou noir, vers lequel le sujet précipite malgré lui, en dehors de ses tentatives de figuration et de symbolisation. Ce trou noir s'ouvre là où l'espace \intersubjectif{} et l'espace \transsubjectif{} ont été déstructurés~: ici, pulsions de vie et \pulsionsdemort{} n'arrivent plus à s'intriquer, et Thànatos se matérialise dans l'\absurde{} d'une \hostilite{} \guil{pure}, dans des \textit{\machinesdeguerre{}} et dans des \textit{zones de mise à mort}. Lorsque le \guil{tiers} reste silencieux face à ces figures du \Mal{}, l'\ordaliemigratoire{} n'est qu'un vertige, une chute, dans laquelle le sujet s'abîme avec la nécessité sourde de la matière~: pour reprendre les mots d'Ismaël \guil{on ne pouvait plus se retourner}.

Dans les rencontres cliniques que nous avons parcourues dans ce texte, l'ombre que la \necropolitique{} jette sur la \guil{scène} \preconscient{}e, et l'\textit{absurde} dont elle infiltre la dimension \intersubjective{}, ont eu des conséquences majeures sur les processus transféro-contre-transférentiels. Je pense que la nécessité et la difficulté qui ont caractérisé le travail du groupe (à partir d'une tentative de \guil{faire groupe} qui n'a jamais pu vraiment aboutir), dans les différents territoires où cette démarche d'enquête s'est déroulée, parlent de ces \guil{ruptures} dans le \preconscient{}. L'investissement, de la part de ces sujets, de deux collectifs comme l'\textit{Amphi Z} et le \textit{Collège} -- où \textit{\hospitalite{}} et \textit{\hostilite{}} se confondent, parfois, dans une \ambiguite{} mortifère, et où elles peuvent, à d'autres moments, s'articuler, se penser, et laisser la place à des formations groupales -- est signifiant. La démarche groupale du travail clinique dans lequel je me suis engagée avec les autres intervenants \guil{soignants}, l'est tout autant. Hormis l'étayage d'un groupe, il serait impossible de revivifier la scène \preconscient{}e envahie par la \necropolitique{}. Mais il ne s'agit pas, ici, de faire appel à une groupalité instituée, soutenue par des cadres et des hiérarchies garantissant un minimum de stabilité et de continuité. Il s'agit, au contraire, de se mettre au travail à partir de groupes qui n'en finissent pas de se faire et de se défaire, de groupes \guil{inactuels}, marginaux, éphémères. Comme si, pour le sujet qui a été exposé aux \guil{frontières}, et qui a connu la condition d'\textit{\homosacer{}}, le processus de (re)construire du groupe était une condition nécessaire à la possibilité de trouver une place dans le groupe. Dans la partie méthodologique, nous avons pris en compte l'\textit{inactuel} dont a écrit Nietzsche,

\begin{quote}
\guil{cette force qui permet de se développer hors de soi-même,  d'une façon qui vous est propre,  de transformer et d'incorporer les choses du passé, de guérir et de cicatriser des blessures, de remplacer ce qui est perdu,  de refaire par soi-même des formes brisées.} \parencite*[7]{Nietzsche_DeuxiemeConsiderationInactuelle}
\end{quote}

Je propose de situer cette \guil{force} au niveau du \preconscient{}, au cœur de l'\intersubjectivite{}. Je pense que, au sein de ces groupalités \guil{blessées}, le sujet issu de l'\ordaliemigratoire{} vient chercher un espace \interstitiel{}, afin d'entamer un \travaildexil{}. Tout le sens de la création d'un \guil{refuge dans le refuge}, toute la valeur de bricoler des praxis de soin, me semble reposer en ceci~: pour se restructurer, au-delà des trous noirs qui ont déterminé l'ordalie, la scène \preconscient{}e a besoin d'un portage groupal. Le caractère \textit{inactuel} du groupe qui accueille le sujet, et que le sujet décide d'accueillir, permet d'inscrire, dans l'espace de la rencontre un mouvement de \revoltesubjective{}, ou, plus précisément, l'interférence de plusieurs mouvements de révolte. C'est, il me semble, à partir de cette inscription, que la \textit{\geographiepsychique{}} (\preconscient{}e) du sujet peut transformer ses dynamiques, cartographiant des chemins susceptibles de l'amener vers l'Autre, l'ailleurs et l'avenir. De telles dynamiques \guil{géographiques} -- \textit{errance}, \textit{ordalie}, \textit{\autoengendrementtraumatique{}}, \textit{\alloengendrement{}}, \textit{révolte} et \textit{\travaildexil{}} -- dessinent des déplacements, des dépaysements, dans l'espace-temps de la rencontre clinique. La \guil{\frontiereinterieure{}}, on la traverse ensemble, ou on ne la traverse pas.

\section{Continuer le chemin}

Pour que l'exploration ouverte par cette thèse ne finisse pas dans une impasse, pour que ce long travail puisse \guil{allo-engendrer} quelque chose, je souhaite mettre en exergue l'intrication entre les hypothèses et les observations cliniques qui sont au cœur de cette enquête. Ainsi, j'espère éclaircir quelques pistes d'élaboration suffisamment solides -- des pistes que d'autres pourront parcourir.

Dans ce temps d'après-coup de la praxis clinique et de l'\ecriture{}, que reste-t-il des hypothèses, si denses et complexes, qui vectorisent cette élaboration -- et dont la répétition a parfois alourdi la lecture de ce manuscrit~? Quels sont les territoires \guil{en recherche} qui les ont faites \guil{travailler}, qui les ont mises à l'épreuve~? Quel pourrait être leur intérêt, pour les chercheurs et les chercheuses qui voudraient approfondir l'approche proposée~?

\subsection{H1. L'ordalie migratoire, entre histoire et géographie du sujet}
\notion{Ordalie migratoire}

    \begin{itemize}
    \item \textbf{Sous-hypothèse 1.1}~: \textit{La \textit{\geographiepsychique{}} ouverte par l'élan d'\guil{\alloengendrement{}} est travaillée par l'Histoire, singulière et collective.}

    Au fil des rencontres cliniques, j'ai été régulièrement confrontée au poids des restes non-historisés du passé transgénérationnel du sujet, et au rôle déterminant de ces \guil{restes}, dans le/la \guil{geste} ordalique. L'ouverture offerte par l'élan d'\guil{\alloengendrement{}} ne va pas du tout de soi. La \guil{\geographiepsychique{}} qui sous-tend les mouvements migratoires est, tout d'abord, une \guil{géographie} pauvre de cartes et de boussoles, car l'environnement primaire du sujet ne s'est pas prêté au jeu d'une \guil{cartographie} vitale, étayant le \preconscient{}. Il s'agit alors, au travers du \gesteordalique{} (et au prix de la perte qu'il entraîne inévitablement), de tenter d'ouvrir à une possibilité d'allo-engen\-drement une \guil{géographie} fermée par un fantasme d'\autoengendrementtraumatique{}. Autrement dit, à partir d'un mouvement circulaire d'\textit{errance}, il est question, pour le sujet, de tracer un vecteur d'\textit{exil}, permettant ainsi à une histoire enfermée dans une répétition mortifère de se mettre en tension avec un à-venir.


    \item \textbf{Sous-hypothèse 1.2}~: \textit{Le trajet migratoire témoigne d'une tentative d'appropriation de ce qui se présente initialement comme inappropriable.}

    Par le mouvement qu'il inscrit dans l'espace extérieur, le sujet se met en quête d'un espace intérieur habitable -- un \guil{ailleurs} qu'il arrive à faire exister en lui, et qu'il projette dehors. Aux prises avec l'éternel \guil{ici et maintenant} d'un traumatisme \guil{retranché}, et avec les \guil{tremblements de temps} qui en découlent, la préservation d'un tel \guil{ailleurs} relève, pour le sujet, d'un enjeu vital. Tant que la coordonnée de l'\textit{ailleurs} reste ouverte, le sujet peut espérer s'émanciper d'une condition existentielle d'exclu.

    \item \textbf{Sous-hypothèse 1.3}~: \textit{L'\guil{\ordaliemigratoire{}} permet au sujet de se déprendre d'un espace incapable d'accueillir, et d'ébaucher une représentation des catastrophes qu'il porte en lui. Mais ce/cette \guil{geste} signale, également, la grande souffrance de la fonction du \preconscient{} et du travail de l'\intersubjectivite{}~: sous l'emprise d'une \necropolitique{}, rien ne \guil{tient} le sujet, rien ne le \guil{contient}, et ainsi il se laisse glisser dans le monde, abandonné, en dehors des coordonnées qui pourraient donner un sens à l'espace.}

    Le/la \guil{geste} ordalique se déroule au plus vif d'une tension, d'une indécision des forces psychiques du sujet~: \textit{\autoengendrementtraumatique{}} ou \textit{\alloengendrement{}}~? Capitulation subjective face à la \miseauban{} et au domaine de la \necropolitique{}, effondrement du \preconscient{} et de l'\intersubjectivite{}, ou tentative signifiante, en quête d'adresse, préfiguration d'une possibilité de penser les catastrophes et d'en témoigner~? Impossible de trancher~: \textit{l'ordalie est pour l'Autre et pour la mort.}
    \end{itemize}

    \subsection{H2. Hors du monde, dans le monde~: déplacements dans la rencontre clinique}

    \begin{itemize}
    \item \textbf{Sous-hypothèse 2.1}~: \textit{La menace d'exclusion du monde humain et la frontière entre vie et mort sont sans cesse convoquées dans les processus transféro-contre-transférentiels de la rencontre clinique.}

    Les accompagnements cliniques que nous avons reparcourus ensemble dans cette thèse nous ont amenés au plus près de cette menace d'exclusion, et des problèmes que pose cette frontière. Lorsque le sujet n'a pas pu trouver sa place parmi ses \guil{autres}, morts ou vivants, et lorsqu'il a été exposé à une déshumanisation de la vie, de la mort, et de la frontière qui les articule, il ne sait pas où se mettre, il a du mal à intriquer, en lui, Éros et Thànatos. Aux côtés d'Akamere, Ismaël et Badou, de telles problématiques ont eu une place tout à fait centrale dans le travail clinique. En fait, j'ai dû faire face à ces aspérités tout au long de mon engagement auprès de l'\textit{Amphi Z} et du \textit{Collège}~: la menace d'exclusion de la commune humanité et la difficulté de vivre sous l'emprise de Thànatos font partie du quotidien de ces collectifs.

    \item \textbf{Sous-hypothèse 2.2}~: \textit{Les cliniciens sont appelés à étayer une \guil{\fonctiontemoin{}}, une fonction d'\guil{\adulteresponsable{}} et un mouvement de \guil{révolte psychique}.}

    Ces deux \guil{fonctions} (qui prennent appui sur le \guil{\temoininterne{}}, sur les \guil{objets phares}, et sur les \guil{\objetsasauver{}} du sujet) et ce \guil{mouvement} (qui concerne une \guil{pulsion anarchiste}) ont pu émerger dans le cadre des permanences psychologiques. Je voudrais souligner encore une fois que ces trois dynamiques ne peuvent se déployer que dans l'\intersubjectivite{} de la rencontre clinique, et qu'ils deviennent sensibles lorsqu'on se met à l'écoute des \guil{interférences} entre cliniciens, sujets accompagnés et contexte du dispositif.

    \item \textbf{Sous-hypothèse 2.3}~: \textit{Le travail clinique consiste donc à chercher, avec le sujet, une possibilité d'exil, préalable à ce qu'un \guil{asile psychique} puisse prendre forme.}

    S'engageant dans un tel travail, le clinicien sera confronté aux mouvements d'aller-retour, aux ruptures et aux remises en cause du cadre qui caractérisent la prise en charge (et qui demandent la \textit{prise en compte}) des sujets \guil{\irreductibles{}}. Un combat vital occupe ces sujets~: la lutte contre l'\textit{absurde}. Afin d'investir une posture de \guil{\therapon{}}, le clinicien doit être disponible pour entrer dans la \guil{tranchée}, aux côtés du sujet (le héros/héraut du combat). Le \travaildexil{} psychique commence donc ainsi, aux prises avec le tumulte d'un champ de bataille.

    \end{itemize}

    \subsection{H3. Hostilité~: glisser dans un monde absurde}
    \notion{Hostilité}

    \begin{itemize}
    \item \textbf{Sous-hypothèse 3.1}~: \textit{Le \travaildelexil{} psychique inhérent à la migration est entravé par les \textit{\processussanssujet{}} traversant le politique contemporain. De tels processus relèvent d'une forme social-historique d'«~absurde~», et assignent le sujet en migration au statut de \guil{\homosacer{}}.}

   La \guil{\pertemassivedhabitat{}}, l'\guil{expulsion} et la \guil{sacralisation} comme paradigmes du traitement politique des figures de l'autre, et une indifférence du \guil{tiers} face à la spoliation et à la destruction de l'humain caractérisent les processus macro-sociaux de \guil{notre} civilisation, dénouant les fils de la \Kulturarbeit{} et mettant à mal les \guil{métacadres} qui rendent possibles les liens. Le monde \textit{absurde} qui en découle est celui où le sujet en migration ne trouve aucune réponse à ses \demandesdasile{}.

    \item \textbf{Sous-hypothèse 3.2}~: \textit{Un tel statut d'\guil{\exception{}}, qui est négation d'une place parmi les autres, vient redoubler, au niveau du lien social, une dynamique psychique que les sujets engagés dans un \travaildexil{} ont déjà expérimentée, dans le cadre de leurs premières relations objectales et identificatoires. Ici, le sujet a été confronté à une impossibilité de s'inscrire dans les traces de l'Autre, et de les transformer.}

    L'\textit{\hostilite{} politique} et l'\textit{\hostilite{} psychique} auxquelles le sujet en situation d'exil est exposé sont profondément liées entre elles, elles semblent se déclencher mutuellement, et leurs \guil{détonations} font voler en éclats le \textit{monde humain}. Dans ces conditions, le sujet se trouve enfermé hors \heritage{} et hors \transmission{}, et, assigné à l'\guil{\exception{}}, il ne peut pas devenir \guil{un parmi d'autres}.

    \end{itemize}

    \subsection{H4. Hospitalité~: bricoler des \guil{institutions inactuelles}}
    \notion{Hospitalité}

    \begin{itemize}
    \item \textbf{Sous-hypothèse 4.1}~: \textit{En marge et en contre de ces tendances macro-sociales, s'organisent des groupes où le mouvement instituant semble prioritaire par rapport au déjà institué~: l'apparent oxymore d'\textit{\institutionsinactuelles{}} peut en rendre compte.}

    S'impliquant dans la vie de ces \textit{\institutionsinactuelles{}}, le sujet peut \guil{refaire des formes brisées} -- pas vraiment \guil{par soi-même}, comme dans la formule nietzschéenne, mais \textit{par les autres et par l'ailleurs}. Le risque de l'utopie est toujours présent, dans ces collectifs qui \guil{militent pour l'accueil}, et qui ont du mal à accéder à une position dépressive, nécessaire à remettre en cause les scléroses des \ideologie{}s groupales. D'où l'importance de soutenir une pensée auto-réflexive, solidaire du \textit{travail du \feminin{}}~: seulement ainsi, le \textit{travail de l'inactuel} est susceptible d'ouvrir des interstices -- des zones de \textit{repos}, \textit{\etonnement{}} et \textit{\transitionnalite{}} -- où il est possible de bricoler de l'accueil.

    \item \textbf{Sous-hypothèse 4.2}~: \textit{Un travail clinique peut ainsi se dérouler au niveau de l'\guil{accompagnement de la vie ordinaire} de ces groupes \guil{exceptionnels}~: voici un deuxième oxymore, qui parle très précisément de la tension qui sous-tend le \gesteordalique{} des sujets qui constituent de tels groupes.}

    La fonction du \textit{\choeur{}} est, à cette endroit, essentielle. Seulement le chant polyphonique qui tisse une narration chorale, avec sa force \intersubjective{} et sa référence au \guil{tiers}, est à même de mettre au travail les oxymores de l'\textit{\hospitalite{}} (\textit{\institutionsinactuelles{}}, \textit{accompagnement de la vie ordinaire de groupes exceptionnels}), sans les réduire. L'univocité n'offre pas de terreau fertile aux praxis de l'\textit{\hospitalite{}}. Au-delà de l'ordalie, le sujet est en quête d'une pluralité de voix.

    \end{itemize}

\section{Perspectives sur une civilisation}

Dans \guil{Espèces d'espaces}, Georges Perec s'attache à décrire les espaces dans lesquels nous vivons~: il commence par la \textit{page}, puis il passe au \textit{lit}, à la \textit{chambre}, à l'\textit{appartement}, à l'\textit{immeuble}, et, lorsqu'il parvient à observer la \textit{rue}, il commence comme ça~:

\begin{quote}
\guil{Les immeubles sont à côté les uns des autres. Ils sont alignés. Il est prévu qu'ils soient alignés, c'est une faute grave pour eux quand ils ne sont pas alignés~: on dit alors qu'ils sont frappés d'alignement, cela veut dire que l'on est en droit de les démolir, afin de les reconstruire dans l'alignement des autres.} \parencite*[93]{Perec_Especes_Espaces}
\end{quote}

Il me semble entendre résonner, dans ces lignes, une métaphore de la civilisation, et notamment, des processus qu'une civilisation met en acte vis-à-vis des sujets qui ne s'alignent pas à ses dogmes -- les \guil{dérangeurs}, les \guil{\irreductibles{}}, ceux et celles qui se soulèvent contre l'\textit{absurde} -- ou qui le souffrent, vivant comme abandonnés dans le monde. \Mesinscription{}, exclusion, mis au ban, \necropolitique{}\ldots La liste des méthodes de \guil{démolition} de ce qui, dans l'humain, ne s'aligne pas, est malheureusement assez longue.

Notre exploration des \ordaliesmigratoires{} nous a permis de penser de telles impasses civilisationnels, attaquant l'altérité et érodant la \Kulturarbeit{}. J'espère avoir montré l'intérêt de telles problématiques pour la psychanalyse contemporaine, qui est appelée à rendre compte des points d'articulation entre deux modalités de production de l'humain travaillant l'une contre l'autre~: les \textit{\processussanssujet{}}, qui relèvent d'un mouvement \textit{absurde} de \massification{} des foules, et l'\textit{\intersubjectivite{}}, qui permet aux sujets de se rencontrer dans un espace pluriel. Je crois qu'il est de plus en plus important de se mettre au travail, en cliniciens et en chercheurs, autour de ces questions, face aux tendances \guil{autophages} qui menacent, aujourd'hui, la capacité de faire place à l'Autre. Hormis un tel effort d'élaboration (qui se doit d'être collectif), les \guil{étoiles} qui guident les explorateurs humains que nous sommes ne peuvent que s'effondrer, générant des trous noirs dont aucun vecteur d'ailleurs et d'avenir ne saurait s'échapper.

Le traitement \necropolitique{} des sujets en migration rappelle l'Europe à ses fantômes. Le fonctionnement des frontières en \guil{\machinesdeguerre{}} -- un fonctionnement accepté comme un état de faits, parfaitement \guil{aligné} aux courants les plus \textit{absurdes} de Thànatos, et provoquant, en toute bonne conscience, la mort de milliers de personnes dans l'indifférence générale -- a déjà produit des traumatismes collectifs irréparables~:

\begin{quote}
\guil{Les frontières sont des lignes. Des millions d'hommes sont morts à cause de ces lignes. Des milliers d'hommes sont morts parce qu'ils ne sont pas parvenus à les franchir~: la survie passait alors par le franchissement d'une simple rivière, d'une petite colline, d'une forêt tranquille~: de l'autre côté, c'était la Suisse, le pays neutre, la zone libre\ldots} \parencite[147]{Perec_Especes_Espaces}
\end{quote}

Malgré toute sa rhétorique de la \guil{mémoire}, l'Europe des années 2020 semble souffrir d'une grave amnésie, quant à ces \guil{lignes} qui tuent ou qui laissent mourir des êtres humains. L'Histoire continue ainsi, dans une répétition des catastrophes~: c'est précisément ce mécanisme macabre, que la pensée psychanalytique est maintenant appelée à élaborer. Seulement ainsi, il sera possible d'ouvrir des marges -- dans l'inconscient et dans le politique -- à même de déjouer cette inertie, cette paresse, cette catastrophe des liens.

Si les effets des \textit{\processussanssujet{}}, qui opèrent à grande échelle, sont plus \guil{spectaculaires} (catastrophe environnementale, destruction du vivant, guerres, hypercapitalisme\ldots), la puissance de l'\textit{\intersubjectivite{}} découle de l'attention portée à l'\textit{\infraordinaire{}}, aux \textit{interstices} de nos vies. Des questions apparemment simples viennent nourrir nos relations avec ces espaces-entre. Par exemple~: \textit{qu'est-ce qu'une ville~?}

\begin{quote}
\guil{Une ville~: de la pierre, du béton, de l'asphalte. Des inconnus, des monuments, des institutions. Mégalopoles. Villes tentaculaires. Artères. Foules. Fourmilières~? Qu'est-ce que le cœur d'une ville~? L'âme d'une ville~?} \parencite[121]{Perec_Especes_Espaces}
\end{quote}

M'impliquant dans cette recherche, j'ai été amenée à me mettre à l'écoute de l'âme marginale de la ville, celle des collectifs qui \guil{ouvrent} des bâtiments qui avaient été fermés à jamais, inventant des formes de vie dans des espaces \guil{déviabilisés}. Je pense que, auprès de ces groupes révoltés, l'œuvre du \feminindeliaison{} peut étayer des praxis se construisant au plus vif de la tension entre \hospitalite{} et \hostilite{}. Ici, entre \travaildexil{} et \travaildelaculture{}, de nouvelles formes de vie, faisant brèche dans les zones nécrosées de la civilisation, peuvent s'esquisser. La pensée qui surgit de ces formes de vie, est une pensée de l'aventure et de l'à-venir.
